#!/bin/sh -e
#
#  uptime: condensed uptime of the machine
#  Copyright (C) 2009 Raphaël Pinson.
#  Copyright (C) 2009 Canonical Ltd.
#  Copyright (C) 2009 Nusku Networks
#
#  Authors: Raphaël Pinson <raphink@ubuntu.com>
#           Dustin Kirkland <kirkland@canonical.com>
#  Authors: Michael Crosson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [ `uname` = "Darwin" ]; then
	u=$(uptime | sed s/,// | awk '{split($5,a,":"); print ($3*24*60 + a[1]*60 + a[2])*60}')
elif [ `uname` = "FreeBSD" ]; then
	u=$(uptime | sed s/,// | awk '{split($3,a,":"); print (a[0]*26*60*60 + a[1]*60*60 + a[2]*60)}')
else
	u=$(sed "s/\..*$//" /proc/uptime)
fi

if [ "$u" -gt 86400 ]; then
	echo "$u" | awk '{printf "%dd%dh", $1 / 86400, ($1 % 86400)/3600 }'
elif [ "$u" -gt 3600 ]; then
	echo "$u" | awk '{printf "%dh%dm", $1 / 3600, ($1 % 3600 )/60}'
elif [ "$u" -gt 60 ]; then
	echo "$u" | awk '{printf "%dm", $1 / 60 }'
else
	printf "%ds" "$u"
fi
