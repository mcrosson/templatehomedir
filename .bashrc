# Paths
#export PYTHONPATH="${PYTHONPATH}:~/opt/lib/python"
export PYTHONPATH="/home/mcrosson/opt/lib/python"
export PYTHONIOENCODING="utf8"
export PYTHONSTARTUP="/home/mcrosson/.python"
export PATH="~/opt/bin:${PATH}:/sbin:/usr/sbin"

# Aliases
alias ping="ping -c4"
alias rsync="rsync --exclude=.DS_Store"
alias ls='ls -F -G --color=auto'
alias grep='grep --colour=auto'
alias screen='tmux'
alias setfacl='/home/mcrosson/opt/bin/setfacl'

# Environment variables
export EDITOR='vim'
export PAGER='less'

# standard gentoo promt (my preferred)
if [ `/usr/bin/whoami` = 'root' ]
then
        # Do not set PS1 for dumb terminals
        if [ "$TERM" != 'dumb'  ] && [ -n "$BASH" ]
        then
                export PS1='\[\033[01;31m\]\h \[\033[01;34m\]\W \$ \[\033[00m\]'
        fi
else
        # Do not set PS1 for dumb terminals
        if [ "$TERM" != 'dumb'  ] && [ -n "$BASH" ]
        then
                export PS1='\[\033[01;32m\]\u@\h \[\033[01;34m\]\W \$ \[\033[00m\]'
        fi
fi

# Ensure prompt is always set at column 1
export PS1="\[\033[G\]$PS1"

# Change the window title of X terminals 
case ${TERM} in
	xterm*|rxvt*|Eterm|aterm|kterm|gnome)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007"'
		;;
	screen)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\033\\"'
		;;
esac

# Turn on history appending
#     Prevents "last logout wins issue"
shopt -s histappend

# Ensure new history gets written after each command
#     This will ensure history doesn't get lost
export PROMPT_COMMAND="history -a"
