"set background=light
hi clear
"if exists("syntax on")
syntax reset
"endif

let colors_name = "IDLE"


if &background == "dark" 
	hi Normal		guifg=White		ctermfg=White
	hi Constant		guifg=Green		ctermfg=Green
	hi Identifier	guifg=Black		ctermfg=Black
	hi Function		guifg=Blue		ctermfg=Blue
	hi Statement	guifg=Orange	ctermfg=Yellow
	hi PreProc		guifg=Orange	ctermfg=Yellow
	hi Type			guifg=Orange	ctermfg=Yellow
	hi NonText		guifg=Blue		ctermfg=Blue
	hi Comment		guifg=Gray		ctermfg=Gray
	
	" TeX Stuff since the default junk is not right
	hi texCmdName		guifg=Purple
	hi texComment		guifg=Gray
	"hi texDef			guifg=
	"hi texDefParm		guifg=
	"hi texDelimiter	guifg=
	"hi texInput		guifg=
	hi texInputFile		guifg=White
	hi texLength		guifg=White
	"hi texMath			guifg=
	hi texMathDelim		guifg=Green
	hi texMathOper		guifg=Green
	"hi texNewCmd		guifg=
	"hi texNewEnv		guifg=
	"hi texOption		guifg=
	"hi texRefZone		guifg=
	hi texSection		guifg=White
	"hi texSpaceCodeChar	guifg=
	hi texSpecialChar	guifg=White
	hi texStatement		guifg=Orange
	"hi texString		guifg=
	"hi texTodo		guifg=
	hi texType		guifg=Purple
	hi texZone		guifg=Purple

else
	hi Normal		guifg=Black
	hi Constant		guifg=DarkGreen
	hi Identifier	guifg=Black	
	hi Function		guifg=Blue
	hi Statement	guifg=DarkOrange
	hi PreProc		guifg=DarkOrange
	hi Type			guifg=DarkOrange
	hi NonText		guifg=Blue
	hi Comment		guifg=DarkGray
	
	" TeX Stuff since the default junk is not right
	hi texCmdName			guifg=Purple
	hi texComment			guifg=DarkGray
	"hi texDef				guifg=
	"hi texDefParm			guifg=
	"hi texDelimiter		guifg=
	"hi texInput			guifg=
	hi texInputFile			guifg=Black
	hi texLength			guifg=Black
	"hi texMath				guifg=
	hi texMathDelim			guifg=DarkGreen
	hi texMathOper			guifg=DarkGreen
	"hi texNewCmd			guifg=
	"hi texNewEnv			guifg=
	"hi texOption			guifg=
	"hi texRefZone			guifg=
	hi texSection			guifg=Black
	"hi texSpaceCodeChar	guifg=
	hi texSpecialChar		guifg=Black
	hi texStatement			guifg=DarkOrange
	"hi texString			guifg=
	"hi texTodo				guifg=
	hi texType				guifg=Purple
	hi texZone				guifg=Purple

endif

hi Error		guifg=White	guibg=Red
hi LineNr		guifg=Blue
