" Pathogen settings.
call pathogen#runtime_append_all_bundles()
"filetype plugin indent on

" have syntax highlighting in terminals which can display colours:
syntax on

" Line numbering
set nu

" don't make it look like there are line breaks where there aren't:
set nowrap

" Set autoindent and tabstop size
set tabstop=4
"set autoindent

" normally don't automatically format `text' as it is typed, IE only do this
" with comments, at 79 characters:
"set formatoptions-=t
"set textwidth=79

" enable filetype detection:
filetype on

" make searches case-insensitive, unless they contain upper-case letters:
set ignorecase
set smartcase

" show the `best match so far' as search strings are typed:
"set incsearch

" assume the /g flag on :s substitutions to replace all matches in a line:
set gdefault


" have <Tab> (and <Shift>+<Tab> where it works) change the level of
" indentation:
"inoremap <Tab> <C-T>
"inoremap <S-Tab> <C-D>
" [<Ctrl>+V <Tab> still inserts an actual tab character.]

"hi Normal ctermbg=black ctermfg=gray

set background=dark

colorscheme elflord

set encoding=utf-8
set showmode
set showcmd
set title

set ignorecase
set smartcase
set incsearch
set showmatch
set hlsearch

"set listchars=tab:.\ ,eol:¬

" jj For Qicker Escaping between normal and editing mode.
inoremap jj <ESC>


" Working with split screen nicely
" Resize Split When the window is resized"
au VimResized * :wincmd =

" Mapping to NERDTree
nnoremap <C-n> :NERDTreeToggle<cr>

" Mini Buffer some settigns."
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1


" Tagbar key bindings."
nmap <leader>l <ESC>:TagbarToggle<cr>
imap <leader>l <ESC>:TagbarToggle<cr>i
